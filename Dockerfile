FROM python:3
COPY . /dockerWorkshop
WORKDIR /dockerWorkshop
RUN pip install -r requirements.txt
EXPOSE 8000
CMD python /dockerWorkshop/server.py